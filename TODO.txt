
[ ] Next version 18.0.1




[ ] Refactor
	[*] Separate common functions into file.
	[ ] Use StdLib instead of hardcoded functions.
	[*] Separate changes into files per mod / topic.
	[*] Don't check for things that should exist / check for the mod only once.

[ ] Check Shiny Icons



[ ] Crawler Train
	[ ] Check Shiny ordering.
	[ ] Check sound.
	[ ] Check balance.
		[ ] Fuel
		[ ] Speed
		[ ] Accel

