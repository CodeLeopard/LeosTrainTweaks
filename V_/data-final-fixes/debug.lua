if false then -- For debug when it does not work.
	log(serpent.block(data.raw["item-with-entity-data"]["locomotive"]))
	log(serpent.block(data.raw["item-with-entity-data"]["cargo-wagon"]))
	log(serpent.block(data.raw["item-with-entity-data"]["fluid-wagon"]))

	log(serpent.block(data.raw["item-with-entity-data"]["bob-locomotive-2"]))
	log(serpent.block(data.raw["item-with-entity-data"]["bob-cargo-wagon-2"]))
	log(serpent.block(data.raw["item-with-entity-data"]["bob-fluid-wagon-2"]))

	log(serpent.block(data.raw["item-with-entity-data"]["bob-locomotive-3"]))
	log(serpent.block(data.raw["item-with-entity-data"]["bob-cargo-wagon-3"]))
	log(serpent.block(data.raw["item-with-entity-data"]["bob-fluid-wagon-3"]))

	log(serpent.block(data.raw["item-with-entity-data"]["petro-locomotive-1"]))
	log(serpent.block(data.raw["item-with-entity-data"]["petro-tank1"]))
	log(serpent.block(data.raw["item-with-entity-data"]["petro-tank2"]))

	log(serpent.block(data.raw["item-with-entity-data"]["smelting-locomotive-1"]))
	log(serpent.block(data.raw["item-with-entity-data"]["smelting-locomotive-tender"]))
	log(serpent.block(data.raw["item-with-entity-data"]["smelting-wagon-1"]))
end