local Util = require "util"

if not mods["ShinyIcons"] then return end

-- fix the shiny mess
log("Fixing Shiny item ordering.")
Util.ShinySortOrder("shinyloco1",  "c3", "petro-locomotive-1");
Util.ShinySortOrder("shinywagon2", "c2", "petro-tank1");
Util.ShinySortOrder("shinywagon2", "c3", "petro-tank2");

Util.ShinySortOrder("shinyloco1",  "c1", "smelting-locomotive-1");
Util.ShinySortOrder("shinyloco1",  "c2", "smelting-locomotive-tender");
Util.ShinySortOrder("shinywagon1", "c1", "smelting-wagon-1");