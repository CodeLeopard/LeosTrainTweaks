local Util = require "util"

-- Settings
local mk1Speed = Util.Speeds.mk1
local mk2Speed = Util.Speeds.mk2
local mk3Speed = Util.Speeds.mk3
local mk4Speed = Util.Speeds.mk4

--###########################################################################

if not mods["boblogistics"] then return end


Util.SetSpeed("locomotive", "bob-locomotive-2", mk2Speed);
Util.SetSpeed("cargo-wagon", "bob-cargo-wagon-2", mk3Speed);
Util.SetSpeed("fluid-wagon", "bob-fluid-wagon-2", mk3Speed);
Util.SetSpeed("artillery-wagon", "bob-artillery-wagon-2", mk2Speed);

Util.SetSpeed("locomotive", "bob-locomotive-3", mk3Speed);
Util.SetSpeed("cargo-wagon", "bob-cargo-wagon-3", mk3Speed);
Util.SetSpeed("fluid-wagon", "bob-fluid-wagon-3", mk3Speed);
Util.SetSpeed("artillery-wagon", "bob-artillery-wagon-3", mk3Speed);