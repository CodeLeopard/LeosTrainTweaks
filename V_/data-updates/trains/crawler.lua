local Util = require "util"

-- Settings
local mk1Speed = Util.Speeds.mk1
local mk2Speed = Util.Speeds.mk2
local mk3Speed = Util.Speeds.mk3
local mk4Speed = Util.Speeds.mk4

--###########################################################################

if not mods["angelsindustries"] then return end


--- ### Tech ### ---
local tech = data.raw["technology"]["angels-crawler-train"]
local prereq = {
	"nuclear-power",
	"electric-energy-accumulators-1",
	"electric-engine",
}

if data.raw["technology"]["bob-railway-3"] then
	table.insert(prereq, "bob-railway-3")
else
	table.insert(prereq, "railway")
end
if data.raw["technology"]["nitinol-processing"] then
	table.insert(prereq, "nitinol-processing")
end

tech.prerequisites = prereq
tech.unit =
{
	count = 200,
	ingredients =
	{
		{"science-pack-1", 10},
		{"science-pack-2", 10},
		{"science-pack-3", 10},
	},
	time = 20
}

if data.raw["tool"]["logistic-science-pack"] then
	table.insert(tech.unit.ingredients, {"logistic-science-pack", 1})
end


--- ### Entities ### ---
local entity = data.raw["locomotive"]["crawler-locomotive"]

if data.raw.locomotive["bob-locomotive-3"] then
	Util.CopyThingToEntity(entity, "locomotive", "bob-locomotive-3", "resistances")
	Util.CopyThingToEntity(entity, "locomotive", "bob-locomotive-3", "equipment_grid")
else
	Util.CopyThingToEntity(entity, "locomotive", "locomotive", "resistances")
	Util.CopyThingToEntity(entity, "locomotive", "locomotive", "equipment_grid")
end

entity.max_speed = mk4Speed
entity.max_power = "1600KW"
entity.reversing_power_modifier = 0.9
entity.weight = 4800
entity.braking_force = 20
entity.friction_force = 0.25
entity.burner.fuel_category = "nuclear"
entity.burner.effectivity = 0.8
entity.burner.fuel_inventory_size = 1
entity.burner.burnt_inventory_size = 1
entity.working_sound.sound.filename = "__base__/sound/idle1.ogg"
entity.working_sound.sound.volume = 1.5
entity.working_sound.idle_sound = { filename = "__base__/sound/idle1.ogg", volume = 1.5 }


local entity = data.raw["locomotive"]["crawler-locomotive-wagon"]
if data.raw.locomotive["bob-locomotive-3"] then
	Util.CopyThingToEntity(entity, "locomotive", "bob-locomotive-3", "resistances")
	Util.CopyThingToEntity(entity, "locomotive", "bob-locomotive-3", "equipment_grid")
else
	Util.CopyThingToEntity(entity, "locomotive", "locomotive", "resistances")
	Util.CopyThingToEntity(entity, "locomotive", "locomotive", "equipment_grid")
end

entity.max_speed = mk4Speed
entity.max_power = "2000KW"
entity.reversing_power_modifier = 1
entity.weight = 5200
entity.braking_force = 22
entity.friction_force = 0.25
entity.burner.fuel_category = "nuclear"
entity.burner.effectivity = 0.8
entity.burner.fuel_inventory_size = 1
entity.burner.burnt_inventory_size = 1
entity.working_sound.sound.filename = "__base__/sound/idle1.ogg"
entity.working_sound.sound.volume = 1.5
entity.working_sound.idle_sound = { filename = "__base__/sound/idle1.ogg", volume = 1.5 }



local entity = data.raw["cargo-wagon"]["crawler-wagon"]
if data.raw.locomotive["bob-locomotive-3"] then
	Util.CopyThingToEntity(entity, "cargo-wagon", "bob-cargo-wagon-3", "resistances")
	Util.CopyThingToEntity(entity, "cargo-wagon", "bob-cargo-wagon-3", "equipment_grid")
else
	Util.CopyThingToEntity(entity, "cargo-wagon", "cargo-wagon", "resistances")
	Util.CopyThingToEntity(entity, "cargo-wagon", "cargo-wagon", "equipment_grid")
end

entity.inventory_size = 120
entity.max_health = 2000
entity.weight = 3000
entity.max_speed = mk4Speed
entity.braking_force = 5.5
entity.friction_force = 0.25
entity.air_resistance = 0.005



local entity = data.raw["cargo-wagon"]["crawler-bot-wagon"]
if data.raw.locomotive["bob-locomotive-3"] then
	Util.CopyThingToEntity(entity, "cargo-wagon", "bob-cargo-wagon-3", "resistances")
	--Util.CopyThingToEntity(entity, "cargo-wagon", "bob-cargo-wagon-3", "equipment_grid")
else
	Util.CopyThingToEntity(entity, "cargo-wagon", "cargo-wagon", "resistances")
	--Util.CopyThingToEntity(entity, "cargo-wagon", "cargo-wagon", "equipment_grid")
end

if data.raw["equipment-grid"]["crawler-wagon-robot"] then
	entity.equipment_grid = "crawler-wagon-robot"
end

entity.inventory_size = 120
entity.max_health = 2200
entity.weight = 3500
entity.max_speed = mk4Speed
entity.braking_force = 5.5
entity.friction_force = 0.27
entity.air_resistance = 0.006



--- ### Recipes ### ---

local recipe = data.raw["recipe"]["crawler-locomotive"]
recipe.energy_required = 100
recipe.ingredients = Util.CheckRecipeIngredients({
	{"bob-locomotive-3", 1},
	{"nuclear-reactor", 1},
	{"heat-pipe", 10},
	{"heat-exchanger", 1},
	{"steam-turbine", 1},
	{"accumilator", 10},
	{"electric-engine-unit", 100},
	{"processing-unit", 100},
	{"nitinol-bearing", 50},
	{"nitinol-plate", 100}
})



recipe = data.raw["recipe"]["crawler-locomotive-wagon"]
recipe.energy_required = 100
recipe.ingredients = Util.CheckRecipeIngredients({
	{"bob-locomotive-3", 1},
	{"nuclear-reactor", 1},
	{"heat-pipe", 10},
	{"heat-exchanger", 1},
	{"steam-turbine", 1},
	{"accumilator", 12},
	{"electric-engine-unit", 120},
	{"processing-unit", 120},
	{"nitinol-bearing", 60},
	{"nitinol-plate", 120}
})


recipe = data.raw["recipe"]["crawler-wagon"]
recipe.energy_required = 100
recipe.ingredients = Util.CheckRecipeIngredients({
	{"bob-cargo-wagon-3", 1},
	{"electric-engine-unit", 4},
	{"processing-unit", 10},
	{"nitinol-bearing", 50},
	{"nitinol-plate", 100}
})


recipe = data.raw["recipe"]["crawler-bot-wagon"]
recipe.energy_required = 100
recipe.ingredients = Util.CheckRecipeIngredients({
	{"bob-cargo-wagon-3", 1},
	{"electric-engine-unit", 8},
	{"processing-unit", 100},
	{"nitinol-bearing", 50},
	{"nitinol-plate", 100}
})
