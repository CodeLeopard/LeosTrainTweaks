local Util = require "util"

-- Settings
local mk1Speed = Util.Speeds.mk1
local mk2Speed = Util.Speeds.mk2
local mk3Speed = Util.Speeds.mk3
local mk4Speed = Util.Speeds.mk4

--###########################################################################

if not mods["boblogistics"] then return end
if not mods["angelsaddons-petrotrain"] then return end

local loco3 = data.raw.locomotive["bob-locomotive-3"]
local cargo3 = data.raw["cargo-wagon"]["bob-cargo-wagon-3"]
local fluid3 = data.raw["fluid-wagon"]["bob-fluid-wagon-3"]
--local arty3 = data.raw["artillery-wagon"]["bob-artillery-wagon-3"]

Util.CopyTrainStats(loco3, "locomotive", "petro-locomotive-1");


Util.CopyTrainStats(fluid3, "fluid-wagon", "petro-tank1");
Util.CopyTrainStats(fluid3, "fluid-wagon", "petro-tank2");

-- Update item sort / tab
Util.UpdateSortOrder("bob-locomotive", "petro-locomotive-1");
Util.UpdateSortOrder("bob-fluid-wagon", "petro-tank1");
Util.UpdateSortOrder("bob-fluid-wagon", "petro-tank2");

local recipe = data.raw.recipe["bob-locomotive-3"];
Util.UpdateRecipe(recipe, "petro-locomotive-1");


recipe = data.raw.recipe["bob-fluid-wagon-3"];
if recipe then
	Util.UpdateRecipe(recipe, "petro-tank1");
	Util.UpdateRecipe(recipe, "petro-tank2");
end

data.raw.technology["angels-petro-train"].prerequisites = {"bob-railway-3", "bob-fluid-wagon-3"}