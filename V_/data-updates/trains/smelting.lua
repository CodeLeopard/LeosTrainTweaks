local Util = require "util"

-- Settings
local mk1Speed = Util.Speeds.mk1
local mk2Speed = Util.Speeds.mk2
local mk3Speed = Util.Speeds.mk3
local mk4Speed = Util.Speeds.mk4

--###########################################################################

if not mods["boblogistics"] then return end
if not mods["angelsaddons-smeltingtrain"] then return end

local loco3 = data.raw.locomotive["bob-locomotive-3"]
local cargo3 = data.raw["cargo-wagon"]["bob-cargo-wagon-3"]
local fluid3 = data.raw["fluid-wagon"]["bob-fluid-wagon-3"]
--local arty3 = data.raw["artillery-wagon"]["bob-artillery-wagon-3"]

Util.CopyTrainStats(loco3, "locomotive", "smelting-locomotive-1")
Util.CopyTrainStats(loco3, "locomotive", "smelting-locomotive-tender") Util.TrainCabUnit("smelting-locomotive-tender");

Util.CopyTrainStats(cargo3, "cargo-wagon", "smelting-wagon-1");


-- Update item sort / tab
Util.UpdateSortOrder("bob-locomotive", "smelting-locomotive-1");
Util.UpdateSortOrder("bob-locomotive", "smelting-locomotive-tender");
Util.UpdateSortOrder("bob-cargo-wagon", "smelting-wagon-1");

local recipe = data.raw.recipe["bob-locomotive-3"];
Util.UpdateRecipe(recipe, "smelting-locomotive-1");
Util.UpdateRecipe(recipe, "smelting-locomotive-tender");

recipe = data.raw.recipe["bob-cargo-wagon-3"];
if recipe then
	Util.UpdateRecipe(recipe, "smelting-wagon-1");
end


data.raw.technology["angels-smelting-train"].prerequisites = {"bob-railway-3"}