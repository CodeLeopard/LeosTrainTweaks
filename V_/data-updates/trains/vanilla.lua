local Util = require "util"

-- Settings
local mk1Speed = Util.Speeds.mk1
local mk2Speed = Util.Speeds.mk2
local mk3Speed = Util.Speeds.mk3
local mk4Speed = Util.Speeds.mk4

--###########################################################################




Util.SetSpeed("locomotive",  "locomotive",  mk1Speed);
Util.SetSpeed("cargo-wagon", "cargo-wagon", mk2Speed);
Util.SetSpeed("fluid-wagon", "fluid-wagon", mk2Speed);
Util.SetSpeed("artillery-wagon", "artillery-wagon", mk1Speed);