local Table = require('__stdlib__/stdlib/utils/table')

if not mods["angelsindustries"] then return end

local equipment = data.raw["equipment-grid"]["angels-crawler-bot-wagon"]

equipment.width = 20
equipment.height = 12
Table.merge(equipment.equipment_categories, {"vehicle", "train", "cargo-wagon"}, true)