data:extend(
{
	{
		type = "int-setting",
		name = "LeosMK1TSpeed",
		setting_type = "startup",
		minimum_value = 10,
		default_value = 250,
		maximum_value = 1000,
		per_user = false,
	},
	{
		type = "int-setting",
		name = "LeosMK2TSpeed",
		setting_type = "startup",
		minimum_value = 10,
		default_value = 350,
		maximum_value = 1000,
		per_user = false,
	},
	{
		type = "int-setting",
		name = "LeosMK3TSpeed",
		setting_type = "startup",
		minimum_value = 10,
		default_value = 450,
		maximum_value = 1000,
		per_user = false,
	},
	{
		type = "int-setting",
		name = "LeosMK4TSpeed",
		setting_type = "startup",
		minimum_value = 10,
		default_value = 555,
		maximum_value = 1000,
		per_user = false,
	},
}
)