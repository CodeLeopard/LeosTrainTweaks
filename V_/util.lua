local Is = require("__stdlib__/stdlib/utils/is")


local Util = {}





-- ### Trains ### --

Util.Speeds = {}
Util.Speeds.mk1 = settings.startup["LeosMK1TSpeed"].value / 216;
Util.Speeds.mk2 = settings.startup["LeosMK2TSpeed"].value / 216;
Util.Speeds.mk3 = settings.startup["LeosMK3TSpeed"].value / 216;
Util.Speeds.mk4 = settings.startup["LeosMK4TSpeed"].value / 216; -- For crawler train.
-- the conversion factor is km/h / 216 = train speed
-- Wagons have the speed of one level higher.



Util.cabUnitMultipliers =
{
	max_power = 1.2,
	braking_force = 1.2,
	weight = 1.1,
	max_health = 1.05
}

function Util.CopyTrainStats(source, type, name)
	local s = source
	local d = data.raw[type][name]

	if not s then log("Cannot copy train stats: source: \""..source.name.."\"does not exist."); return end
	if not d then log("Cannot copy train stats: destination: \""..type..".".. name .."\" does not exist."); return end

	if d.type ~= s.type then error("Type mismatch: \""..source.type.."\" ~= \""..type.."\".") end

	-- common stats
	d.max_health = s.max_health;
	d.max_speed = s.max_speed;
	d.weight = s.weight;
	d.braking_force = s.braking_force;
	d.friction_force = s.friction_force;
	d.air_resistance = s.air_resistance;
	d.resistances = s.resistances;

	if s.equipment_grid then
		d.equipment_grid = s.equipment_grid;
	end

	-- type specific stats
	if d.type == "locomotive" then
		d.max_power = s.max_power;
		d.reversing_power_modifier = s.reversing_power_modifier;

		d.burner.effectivity = s.burner.effectivity;
		d.burner.fuel_inventory_size = s.burner.fuel_inventory_size;
	end

	if d.type == "cargo-wagon" then
		d.inventory_size = s.inventory_size;
	end

	if d.type == "fluid-wagon" then
		d.capacity = s.capacity;
	end

	if d.type == "artillery-wagon" then
		-- No things appropriate for copy.
	end
end


function Util.TrainCabUnit(name)
	local e = data.raw.locomotive[name];
	local m = Util.cabUnitMultipliers;

	if not e then log("Cannot apply no-Cab Upgrade because does not exist: " .. name); return end

	e.weight = e.weight * m.weight;

	local num, suf = Util.PowerStringSep(e.max_power);
	local newPower = (num * m.max_power) .. suf;
	--log("Upping power from: " .. e.max_power .. " to: " .. newPower)
	e.max_power = newPower

	e.max_health    = e.max_health    * m.max_health;
	e.braking_force = e.braking_force * m.braking_force;
end

function Util.SetSpeed(cat, name, speed)
	entity = data.raw[cat][name];
	if entity then
		entity.max_speed = speed;
	end
end


-- ### Recipe ### --

function Util.UpdateRecipe(base, name)
	local edit = data.raw.recipe[name];
	if not edit then log("Cannot update recipe " .. name .. " because it does not exist"); return end

	edit.energy_required = base.energy_required;
	edit.ingredients = base.ingredients;
end

function Util.CheckRecipeIngredients(ingredients)
	local returnIngredients = {}
	for k, v in pairs(ingredients) do
		if data.raw["item"][v.name] then
			table.insert(returnIngredients, v)
		end
	end
	return returnIngredients
end





-- ### Sorting ### --

function Util.UpdateSortOrder(subGroup, name)
	local type = data.raw["item-subgroup"][subGroup];
	local item = data.raw["item-with-entity-data"][name];
	if not (type and name) then
		log ("Skipping Re-Oredering of " .. name);
		return;
	end

	item.subgroup = subGroup;
	item.order = "b[train-system]-f[" .. name .."]";
end


function Util.ShinySortOrder(subGroup, order, name)
	local type = data.raw["item-subgroup"][subGroup];
	local item = data.raw["item-with-entity-data"][name];

	if not (type and name) then
		log ("Skipping Re-Oredering of " .. name);
		return;
	end
	item.subgroup = subGroup;
	item.order = order;
end


--- ### Data editing ### ---

function Util.CopyThingToEntity(entity, categoryFrom, entityNameFrom, thing)
	Is.Assert(entity and entity.valid, "entity was not valid")
	Is.Assert(data.raw.categoryFrom, "data.raw.categoryFrom was not valid")
	Is.Assert(data.raw.categoryFrom.entityNameFrom, "data.raw.categoryFrom.entityNameFrom was not valid")
	Is.Assert(data.raw.categoryFrom.entityNameFrom.thing, "data.raw.categoryFrom.entityNameFrom.thing was not valid")

	entity[thing] = data.raw[categoryFrom][entityNameFrom][thing]
end


function Util.CopyThing(categoryFrom, entityNameFrom, categoryTo, entityNameTo, thing)
	Is.Assert(data.raw.categoryTo, "data.raw.categoryTo was not valid")
	Is.Assert(data.raw.categoryTo.entityNameTo, "data.raw.categoryTo.entityNameTo was not valid")

	Util.CopyThingToEntity(data.raw[categoryTo][entityNameTo], categoryFrom, entityNameFrom, thing)
end


return Util